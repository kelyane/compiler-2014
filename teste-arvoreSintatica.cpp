#include "sintatico-arvore.h"
#include "util-define.h"

int main(){
	dadoToken *dtoken = new dadoToken();
	dtoken->value = "abc";
	no *dno = new no(dtoken);
	
	dadoToken *dtoken2 = new dadoToken();
	dtoken2->value = "abcdef";
	no *dno2 = new no(dtoken2);
	
	no *nttoken = new no(0);
	nttoken->addChild(dno);
	nttoken->addChild(dno2);
	
	no *nttoken2 = new no(0);
	nttoken->addChild(nttoken2);
	
	dadoToken *dtoken3 = new dadoToken();
	dtoken3->value = "abcdefghi";
	no *dno3 = new no(dtoken3);
	
	nttoken2->addChild(dno3);
	
	nttoken->imprimeNo();
	
	delete dno;
	return 0;
}
