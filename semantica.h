#pragma once

#include<string>
#include<iostream>
#include "sintatico-arvore.h"
#include "util-define.h"


bool analiseSemantica(no *root) {

  tipoExpressao tipoExp;
  
  if (root->children[0] == NTEXP) {
    tipoExp = analiseExpressao(root->children[0]);
  }else{    
    std::cout << "analide de DECS nao implementada\n";    
  }
  
  std::cout << "No não avaliado semanticamente\n";    
  return false;
}

tipoExpressao analiseSemanticaIF(no *n){    
  
  
  
  //if then else
  if(n->children[2]->tamChildren == 1){
    
    tipoExpressao exp2 = analiseExpressao(n->children[1]);
    tipoExpressao exp3 = analiseExpressao(n->children[2]);
    
    if (exp2.tipo == exp3.tipo){
      return exp2.tipo;      
    }
    
  }
  
  //if then
  return  tipoExpressao = nullptr  ;
}



tipoExpressao analiseExpressao(no *n){
    
  tipoExpressao tipoExp;
  
  if(n->token != nullptr){
    tipoExp.tipo = n->token->tipo;   
    return tipoExp;
  }
  
  //no nao terminal
  switch(n->tipoNoNaoTeminal){
    case NTIFEXP:{
      return tipoExp = analiseSemanticaIF(n);
      break;
    }
  }
  
  return tipoExp;
}
