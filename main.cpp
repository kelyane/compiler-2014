#include <stdlib.h>
#include <stdio.h>
#include "util-define.h"
#include "sintatico-arvore.h"
#include "sintatico.hpp"
#include "llvm.h"
//#include "semantica.h"

extern FILE * yyin;
extern no *root;

int main( int argc, char **argv ){
  
	++argv, --argc;  
	if ( argc > 0 )
		yyin = fopen( argv[0], "r" );
	else
	    yyin = stdin;
	
	yyparse();
	
	no *r = root;

	if(r==nullptr){
	  std::cout<< "raiz nao definida \n";
	}else{
 	  r->imprimeNo();
	}
	
	geraCodigoLLVM(root);
	
//	analiseSemantica(root);
	
}