 
%{
#include <math.h>
#include <string>
#include <iostream>
#include <stdio.h>
#include "util-define.h"
#include "sintatico-arvore.h"
#include "sintatico.hpp"


int yyRow = 1;
int yyCol = 1;


#define PRINTKW printf(" Token: (%s) linha: %d - coluna: %d \n", yytext, yyRow, yyCol)
#define PRINTSB printf(" Symbol: (%s) linha: %d - coluna: %d \n", yytext, yyRow, yyCol)

#define fillBasicToken() \
     yylval.token = new dadoToken; \
     yylval.token->row = yyRow; \
     yylval.token->col = yyCol; \
     yyCol += yyleng;
     
#define returnToken(t) \
     fillBasicToken() \
     return (t)
 
#define returnLexeme(t) \
     fillBasicToken() \
     yylval.token->value = std::string(yytext, yyleng); \
     return (t)


//void comment();

%}
   
ER_INT  [0-9]
ER_IDENT [a-zA-Z_][a-zA-Z0-9_]*

%%
   
{ER_INT}+ {
	printf( " Inteiro: (%d) linha: %d - coluna: %d \n", atoi( yytext ),yyRow, yyCol );	
	returnLexeme(t_integer);
}
   
{ER_INT}+"."{ER_INT}* { //Não tem float no Tiger
	printf( " Float: (%g) linha: %d - coluna: %d  \n", atof( yytext ),yyRow, yyCol ); 
}

"/*"		{ /*comment();*/ }

"array"		{ PRINTKW; returnToken(t_array);}
"while"		{ PRINTKW; returnToken(t_while);}
"for"		{ PRINTKW; returnToken(t_for);}
"then"		{ PRINTKW; returnToken(t_then);}
"if"		{ PRINTKW; returnToken(t_if);}
"else"		{ PRINTKW; returnToken(t_else);}
"do"		{ PRINTKW; returnToken(t_do);}
"to"		{ PRINTKW; returnToken(t_to);}
"let"		{ PRINTKW; returnToken(t_let);}
"in"		{ PRINTKW; returnToken(t_in);}
"end"		{ PRINTKW; returnToken(t_end);}
"break"		{ PRINTKW; returnToken(t_break);}
"nil"		{ PRINTKW; returnToken(t_nil);}
"var"		{ PRINTKW; returnToken(t_var);}
"type"		{ PRINTKW; returnToken(t_type);}
"class"		{ PRINTKW; returnToken(t_class);}
"new"		{ PRINTKW; returnToken(t_new);}
"method"	{ PRINTKW; returnToken(t_method);}
"extends"	{ PRINTKW; returnToken(t_extends);}
"function"	{ PRINTKW; returnToken(t_function);}
"import"	{ PRINTKW; returnToken(t_import);}
"primitive"	{ PRINTKW; returnToken(t_primitive);}

"\""[.]+"\""	{ 
	PRINTKW; 	
	returnLexeme(t_string);
}

{ER_IDENT}+	{ 
	PRINTKW; 		
	returnLexeme(t_identifier);
}

","		{ PRINTSB; returnToken(t_comma);}
":"		{ PRINTSB; returnToken(t_colon);}
";"		{ PRINTSB; returnToken(t_semicolon);}
"."		{ PRINTSB; returnToken(t_dot);}
"="		{ PRINTSB; returnLexeme(t_eq);}
"!="		{ PRINTSB; returnLexeme(t_neq);}
"{"		{ PRINTSB; returnToken(t_lbrace);}
"}"		{ PRINTSB; returnToken(t_rbrace);}
"("		{ PRINTSB; returnToken(t_lparen);}
")"		{ PRINTSB; returnToken(t_rparen);}
"["		{ PRINTSB; returnToken(t_lbrack);}
"]"		{ PRINTSB; returnToken(t_rbrack);}
"<"		{ PRINTSB; returnLexeme(t_lt);}
">"		{ PRINTSB; returnLexeme(t_gt);}
"<="		{ PRINTSB; returnLexeme(t_le);}
"=>"		{ PRINTSB; returnLexeme(t_ge);}
"+"		{ PRINTSB; returnLexeme(t_plus);}
"-"		{ PRINTSB; returnLexeme(t_minus);}
"*"		{ PRINTSB; returnLexeme(t_time);}
"/"		{ PRINTSB; returnLexeme(t_divide);}
"&"		{ PRINTSB; returnLexeme(t_and);}
"|"		{ PRINTSB; returnLexeme(t_or);}
":="		{ PRINTSB; returnToken(t_assign);}
   
[\n\r]	{ yyRow++; yyCol = 1; }

[ \t]+ { yyCol+=yyleng; }   
.	printf( "Caracter nao reconhecido: %s\n", yytext );

 
%%
   
int yywrap() {}


/*void comment(){
	int c;

  while ((c = input()) != 0)
    if (c == '*')
    {
      while ((c = input()) == '*')        

      if (c == '/')
        return;

      if (c == 0)
        break;
    }
}*/





