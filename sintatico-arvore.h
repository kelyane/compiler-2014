#pragma once

#include <iostream>
#include "util-define.h"
#include <stdlib.h>
#include <stdio.h> 

struct no{
	
	noTerminalToken tipoNoNaoTeminal;
	no* children[20];
	size_t tamChildren;
	dadoToken *token;
	
	void initChildren() {
	  tamChildren = 0;
	  for (int i = 0; i < 20; i++) {
	    children[i] = nullptr;
	  }
	}
	
	no(dadoToken *dtoken){
		tipoNoNaoTeminal = NTNULL;
		initChildren();
		token = dtoken;
	}
	
	
	no(dadoToken *dtoken, std::string tipo){
		tipoNoNaoTeminal = NTNULL;
		initChildren();
		token = dtoken;
		token->tipo = tipo;
	}
	
	no(noTerminalToken dtipoNoNaoTeminal){
		tipoNoNaoTeminal = dtipoNoNaoTeminal;
		initChildren();
		token = NULL;
	}
	
	void addChild(no *dNo){
	        children[tamChildren++] = dNo;		
	}
	
	~no(){
		if(token){
			delete token;
			return;
		}
		
		for(int i = 0; i < tamChildren; i++){
			delete children[i];
		}
		
		delete children;
	}
	
	void imprimeNo(int recuo = 0){
		int i;
		
		for(i=0; i<recuo; i++){
			std::cout << ".";
		}
		
		if(token != NULL){
			std::cout << "value: "<< token->value << "\n";
			return;
		}
		
		std::cout << "no " << tipoNoToLabel[tipoNoNaoTeminal] << "\n";
		
		for(int i = 0; i < tamChildren; i++){
			children[i]->imprimeNo(recuo + 1);
		}				
		
	}
	
};
