project(compiler-example)
 
cmake_minimum_required(VERSION 2.6)
 
find_package(BISON)
find_package(FLEX)

# gambi
include_directories(/usr/include/llvm-3.5/ /usr/include/llvm-c-3.5/)
link_directories(/usr/lib/llvm-3.5/lib/)

add_definitions(${LLVM_DEFINITIONS})

add_definitions(-std=c++11 -Wall)
 
BISON_TARGET(Parser sintatico.y ${CMAKE_CURRENT_BINARY_DIR}/sintatico.cpp VERBOSE verbose.txt)
FLEX_TARGET(Lexer lexico.flex ${CMAKE_CURRENT_BINARY_DIR}/lexico.cpp)
ADD_FLEX_BISON_DEPENDENCY(Lexer Parser)
 
include_directories(${CMAKE_BINARY_DIR})
include_directories(${CMAKE_SOURCE_DIR})
 
add_executable(main main.cpp llvm.cpp ${BISON_Parser_OUTPUTS} ${FLEX_Lexer_OUTPUTS})
target_link_libraries(main LLVM-3.5)
#add_executable(llvm llvm.cpp)
#target_link_libraries(llvm LLVM-3.5)